-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Des 2022 pada 08.33
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tutordee`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2022-12-07-115914', 'App\\Database\\Migrations\\Users', 'default', 'App', 1671081716, 1),
(2, '2022-12-09-081153', 'App\\Database\\Migrations\\Tutorial', 'default', 'App', 1671081716, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `tutorial`
--

CREATE TABLE `tutorial` (
  `tutorid` int(12) UNSIGNED NOT NULL,
  `judul` varchar(100) NOT NULL,
  `author` varchar(12) NOT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `tag` varchar(12) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `tutorial`
--

INSERT INTO `tutorial` (`tutorid`, `judul`, `author`, `isi`, `gambar`, `tag`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Cara Membuat Macaron Tanpa Tepung Almond', 'Admin', '<p>Ada cara membuat macaron tanpa tepung almond. Kamu bisa menggantinya dengan tepung serbaguna. Namun, kamu perlu tahu takaran yang tepat agar hasilnya tidak berujung gagal. Pada resep macaron tanpa tepung almond ini, menggunakan tujuh bahan. Di antaranya, ada putih telur, gula bubuk, dan ekstrak vanila.</p><p>Resep macaron tanpa tepung almond</p><p><strong>Bahan:</strong></p><ul><li>2 butir putih telur</li><li>4 sdm gula pasir, tambah &frac12; sdt</li><li>65 gram tepung serbaguna</li><li>55 gram gula bubuk</li><li>1 sdt ekstrak vanila</li><li>Pewarna makanan secukupnya</li><li>Frosting buttercream secukupnya</li></ul><p><strong>Langkah-Langkah:</strong></p><ol><li>Siapkan mangkuk bersih dan kocok putih telur hingga berbusa.</li><li>Tambahkan gula secara bertahap dan kocok hingga putih telur kaku. Di mangkuk lainnya, aduk tepung serbaguna dan gula bubuk.</li><li>Saring campuran tepung dan gula ke dalam kocokan putih telur. Tambahkan ekstrak vanila dan pewarna makanan, kalau kamu ingin mewarnai macaron.</li><li>Aduk-aduk adonan dengan cara dilipat-lipat. Tuang adonan ke dalam piping bag. Potong ujungnya. Semprotkan adonan macaron di atas kertas roti. Beri jarak.</li><li>Kalau adonan sudah memenuhi loyang, ketuk perlahan agar permukaannya rata dan mengeluarkan gelembung. Diamkan macaron di suhu kamar sekitar 45 menit.</li><li>Panaskan oven pada suhu 138 derajat celsius. Kalau oven sudah panas, panggang selama 17-20 menit.</li><li>Kalau macaron sudah matang, biarkan macaron dingin selama 10 menit. Pindahkan macaron ke rak pendingin.</li><li>Olesi bagian bawah macaron dengan frosting buttercream. Tumpuk dengan macaron lainnya seperti sandwich. Biarkan macaron selama 24 jam sebelum dinikmati agar lebih enak.</li></ol>', '5fe88be892861.jpg', 'Makanan', '2022-12-14 23:27:08', '2022-12-14 23:27:08', NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `username` varchar(12) NOT NULL,
  `nama` varchar(24) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `tutorial`
--
ALTER TABLE `tutorial`
  ADD PRIMARY KEY (`tutorid`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `tutorial`
--
ALTER TABLE `tutorial`
  MODIFY `tutorid` int(12) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
