<?php

namespace App\Controllers;

use App\Models\FormModel;

class Page extends BaseController
{
	public function about()
	{
		echo view("about");
	}
	public function succesfull()
	{
		echo view("succesfull");
	}
	public function tentang()
	{
		echo view("tentang");
	}
	public function tutorials()
	{
		// $data['newses'] = $news->where('status', 'published')->findAll();
		$db = \Config\Database::connect();
		$builder = $db->table('tutorial')->limit(6)->orderBy('tutorid', 'RANDOM');
		$query = $builder->get();
		return view('tutorials', compact('query'));
	}
	public function tutorsarticle($tutorid)
	{
		$db = \Config\Database::connect();
		$builder = $db->query("SELECT * FROM tutorial WHERE tutorid = " . $tutorid . "");
		return view('tutorsarticle', compact('builder'));
	}
	public function tutorafterlogin()
	{
		$db = \Config\Database::connect();
		$builder = $db->table('tutorial')->limit(6)->orderBy('tutorid', 'RANDOM');
		$query = $builder->get();
		return view('tutorafterlogin', compact('query'));
	}
	public function article($tutorid)
	{
		$db = \Config\Database::connect();
		$builder = $db->query("SELECT * FROM tutorial WHERE tutorid = " . $tutorid . "");
		return view('article', compact('builder'));
	}
}
