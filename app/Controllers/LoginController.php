<?php

namespace App\Controllers;

use App\Models\UserModel;

class LoginController extends BaseController
{
    public function index()
    {
        helper(['form']);
        echo view('login');
    }

    public function loginAuth()
    {
        $users = new UserModel();
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');

        $data = $users->where([
            'username' => $username,
        ])->first();
        if ($data) {
            if (password_verify($password, $data->password)) {
                session()->set([
                    'username'      => $data->username,
                    'nama'          => $data->nama,
                    'email'         => $data->email,
                    'isLoggedIn'    => TRUE
                ]);
                $db = \Config\Database::connect();
                $builder = $db->table('tutorial')->limit(6)->orderBy('tutorid', 'RANDOM');
                $query = $builder->get();
                return view('beranda', compact('query'));
            } else {
                session()->setFlashdata('msg', 'Password tidak valid');
                return redirect()->to('/login');
            }
        } else {
            session()->setFlashdata('msg', 'Username tidak valid');
            return redirect()->to('/login');
        }
    }

    public function logout()
    {
        $session = session();
        $session->destroy();
        return redirect()->to('/login');
    }
}
