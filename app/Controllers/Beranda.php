<?php 
namespace App\Controllers;
  
class Beranda extends BaseController
{
    public function index()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('tutorial')->limit(6)->orderBy('tutorid', 'RANDOM');
        $query = $builder->get();
        return view('beranda', compact('query'));
    }
}