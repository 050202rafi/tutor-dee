<?php

namespace App\Controllers;
use App\Models\UserModel;

class Info extends BaseController
{
    public function info()
    {
        helper(['form']);
        $data = [];
        echo view('info', $data);
    }

    public function update(){
        if (!$this->validate([
            'nama' => [
                'rules' => 'required|min_length[4]|max_length[24]',
                'errors' => [
                    'required' => '{field} harus diisi',
                    'min_length' => '{field} minimal 4 Karakter',
                    'max_length' => '{field} maksimal 24 Karakter',
                ]
            ],
            'email' => [
                'rules' => 'required|min_length[4]|max_length[50]',
                'errors' => [
                    'required' => '{field} harus diisi',
                    'min_length' => '{field} minimal 4 Karakter',
                    'max_length' => '{field} maksimal 50 Karakter',
                ]
            ],
            'password' => [
                'rules' => 'required|min_length[4]|max_length[255]',
                'errors' => [
                    'required' => '{field} harus diisi',
                    'min_length' => '{field} minimal 4 Karakter',
                    'max_length' => '{field} maksimal 255 Karakter',
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }
        $userModel = new UserModel();
        $id = $this->request->getVar('username');
        $u = $userModel->where('username',$id)->first();
        $data = [
            'nama' => $this->request->getVar('nama'),
            'email'  => $this->request->getVar('email'),
            'password' => password_hash($this->request->getVar('password'), PASSWORD_DEFAULT),
        ];

        $userModel->update($id, $data);
        return redirect()->to('/info')->with('success', 'Update akun anda berhasil.');
    }
}