<?php
namespace App\Controllers;

class Home extends BaseController
{
    public function index()
    {
        $db = \Config\Database::connect();
        $builder = $db->table('tutorial')->limit(6)->orderBy('tutorid', 'RANDOM');
        $query = $builder->get();
        return view('index', compact('query'));
    }
}
