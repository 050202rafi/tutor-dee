<?php

namespace App\Controllers;

use App\Models\FormModel;
use GdImage;

class Tutorialku extends BaseController
{
    public function tutorialku()
    {
        helper(['form']);
        $data = [];
        echo view('tutorialku', $data);
    }

    public function save()
    {
        if (!$this->validate([
            'judul' => [
                'rules' => 'required|max_length[100]|',
                'errors' => [
                    'required' => '{field} harus diisi',
                    'max_length' => '{field} maksimal 12 Karakter',
                ]
            ],
            'author' => [
                'rules' => 'required|max_length[12]',
                'errors' => [
                    'required' => '{field} harus diisi',
                    'max_length' => '{field} maksimal 12 Karakter',
                ]
            ],
            'isi' => [
                'rules' => 'required',
                'errors' => [
                    'required' => '{field} harus diisi',
                ]
            ],
            'gambar' => [
                'rules' => 'uploaded[gambar]|mime_in[gambar,image/jpg,image/jpeg,image/gif,image/png]|max_size[gambar,4096]',
                'errors' => [
                    'uploaded' => 'Harus Ada File yang diupload',
                    'mime_in' => 'File Extention Harus Berupa jpg,jpeg,gif,png',
                    'max_size' => 'Ukuran File Maksimal 4 MB'
                ]

            ],
            'tag' => [
                'rules' => 'required|max_length[12]',
                'errors' => [
                    'required' => '{field} harus diisi',
                    'max_length' => '{field} maksimal 12 Karakter',
                ]
            ],
        ])) {
            session()->setFlashdata('error', $this->validator->listErrors());
            return redirect()->back()->withInput();
        }

        $gambarnya = $_FILES['gambar']['name'];
        $gambar = new FormModel();

        $dataGambar = $this->request->getFile('gambar');
        $fileName = \Config\Services::image('gd')
            ->withFile($dataGambar)
            ->fit(970, 647, 'center')
            ->save('img/tutor/' . $gambarnya);
        $gambar->insert([
            'judul' => $this->request->getVar('judul'),
            'author' => $this->request->getVar('author'),
            'isi' => $this->request->getVar('isi'),
            'gambar' => $gambarnya,
            'tag' => $this->request->getVar('tag')
        ]);
        return redirect()->to('/tutorialku')->with('success', 'Tutorial berhasil diupload.');
    }
}