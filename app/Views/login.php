<!DOCTYPE html>
<html lang="en" class="no-js">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= base_url('css/styles.css') ?>" />
    <link href='https://cdn.jsdelivr.net/npm/boxicons@2.0.5/css/boxicons.min.css' rel='stylesheet'>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <link href="http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" rel="stylesheet" type="text/css">
    <link href="<?= base_url('simple-line-icons/simple-line-icons.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('css/animate.css') ?>" rel="stylesheet">
    <link href="<?= base_url('swiper/css/swiper.min.css') ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('css/layout.min.css') ?>" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" type="image/png" href="<?= base_url('img/logo-dark.png') ?>" />
    <title>Login</title>
</head>

<?= $this->renderSection('content') ?>

<!--== HEADER ==-->

<body>
    <header class="header navbar-fixed-top" style="background: url(../img/1920x1080/02.jpg) no-repeat; background-size: cover; background-position: center center;">
        <!-- Navbar -->
        <nav class="navbar" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="menu-container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="toggle-icon"></span>
                    </button>

                    <!-- Logo -->
                    <div class="logo">
                        <a class="logo-wrap" href="<?= base_url() ?>">
                            <img class="logo-img logo-img-main" src="<?= base_url('img/logo.png') ?>" alt="Asentus Logo">
                            <img class="logo-img logo-img-active" src="<?= base_url('img/logo-dark.png') ?>" alt="Asentus Logo">
                        </a>
                    </div>
                    <!-- End Logo -->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse nav-collapse">
                    <div class="menu-container">
                        <ul class="navbar-nav navbar-nav-right">
                            <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url() ?>">Home</a></li>
                            <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url('tutorials') ?>">Tutorial</a></li>
                            <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url('about') ?>">About</a></li>
                            <li class="nav-item"><a class="nav-item-login nav-item-hover active" href="<?= base_url('login') ?>">Login</a></li>
                            <li class="nav-item"><a class="nav-item-signin nav-item-hover" href="<?= base_url('register') ?>">Register</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- End Navbar Collapse -->
        </nav>
        <!-- Navbar -->
    </header>
    <div class="container-fluid" style="margin-top: 125px;">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <?php if (!empty(session()->getFlashdata('msg'))) : ?>
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <?php echo session()->getFlashdata('msg'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </div>
                <?php endif; ?>
                <?php if (session()->getFlashdata('success')) : ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <?php echo session()->getFlashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>


    <div class="login">
        <div class="login__content">
            <div class="login__img">
                <img src="<?= base_url('img/img-login.svg') ?>" alt="">
            </div>

            <!-- Judul Login -->
            <div class="login__forms">
                <?= csrf_field(); ?>
                <form action="<?php echo base_url(); ?>/LoginController/loginAuth" method="POST" class="login__registre" id="login-in">

                    <h1 class="login__title">Login</h1>
                    <!-- Untuk Login-->

                    <div class="login__box">
                        <i class='bx bx-user login__icon'></i>
                        <input type="text" name="username" placeholder="Username" id="username" class="login__input" required autofocus>
                    </div>

                    <div class="login__box">
                        <i class='bx bx-lock-alt login__icon'></i>
                        <input type="password" placeholder="Password" class="login__input" name="password" id="password" required>
                    </div>

                    <a href="#" class="login__forgot">Lupa Password</a>

                    <button name="submit" class="login__button">Login</button>

                    <div>
                        <span class="login__account">Tidak Punya Akun ?</span>
                        <a href="<?= base_url('register') ?>"><span class="login__signin" id="sign-up">Buat Akun</span></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--===== MAIN JS =====-->
    <!-- JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- CORE PLUGINS -->
    <script src="<?= base_url('js/main.js') ?>"></script>
    <script src="<?= base_url('jquery/jquery.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('jquery/jquery-migrate.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>

    <!-- PAGE LEVEL PLUGINS -->
    <script src="<?= base_url('jquery/jquery.easing.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('jquery/jquery.back-to-top.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('jquery/jquery.smooth-scroll.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('jquery/jquery.wow.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('masonry/imagesloaded.pkgd.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('masonry/jquery.masonry.pkgd.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('swiper/js/swiper.jquery.min.js') ?>" type="text/javascript"></script>

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url('js/layout.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('js/components/wow.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('js/components/swiper.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('js/components/masonry.min.js') ?>" type="text/javascript"></script>
</body>

</html>