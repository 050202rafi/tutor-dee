<?= $this->extend('layout/page_layout') ?>
<?= $this->section('content') ?>
<?php foreach ($builder->getResult() as $q) { ?>
    <div class="content-lg container">
        <div class="row margin-t-100">
            <h1><?php echo $q->judul ?></h1>
            <h5><span class="text-uppercase margin-l-15">Oleh : <?php echo $q->author ?></span></h5>
            <h5><span class="text-uppercase margin-l-15"><?php echo $q->tag ?></span></h5>
        </div>
        <img class="img-responsive" src="<?= base_url('img/tutor/' . $q->gambar . '') ?>">
        <div class="row margin-b-50">
            <div class="margin-t-50">
                <?php echo $q->isi ?>
            </div>
        </div>
    </div>
<?php } ?>
<?= $this->endSection() ?>