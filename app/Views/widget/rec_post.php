<?php foreach ($query->getResult() as $q) { ?>
    <div class="col-sm-4 sm-margin-b-50">
        <div class="margin-b-20">
            <div class="wow zoomIn" data-wow-duration=".3" data-wow-delay=".1s">
                <img class="img-responsive" src="<?= base_url('img/tutor/' . $q->gambar . '') ?>">
            </div>
        </div>
        <h4><a href="/tutorsarticle/<?= $q->tutorid ?>"><?php echo $q->judul ?></a></h4>
        <p><?= substr($q->isi, 0, 90) ?></p>
        <a class="link" href="/tutorsarticle/<?= $q->tutorid ?>">Read More</a>
        <h5><span class="text-uppercase margin-l-15"><?php echo $q->tag ?></span></h5>
    </div>
<?php } ?>