<header class="header navbar-fixed-top" style="background: url(../img/1920x1080/02.jpg) no-repeat; background-size: cover; background-position: center center;">
    <nav class="navbar" role="navigation">
        <div class="container">
            <div class="menu-container">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="toggle-icon"></span>
                </button>

                <!-- Logo -->
                <div class="logo">
                    <a class="logo-wrap" href="<?= base_url() ?>">
                        <img class="logo-img logo-img-main" src="<?= base_url('img/logo.png') ?>" alt="Asentus Logo">
                        <img class="logo-img logo-img-active" src="<?= base_url('img/logo-dark.png') ?>" alt="Asentus Logo">
                    </a>
                </div>
            </div>

            <div class="collapse navbar-collapse nav-collapse">
                <div class="menu-container">
                    <ul class="navbar-nav navbar-nav-right">
                        <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url() ?>">Home</a></li>
                        <li class="nav-item"><a class="nav-item-child nav-item-hover active" href="<?= base_url('tutorials') ?>">Tutorial</a></li>
                        <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url('about') ?>">About</a></li>
                        <li class="nav-item"><a class="nav-item-login nav-item-hover" href="<?= base_url('login') ?>">Login</a></li>
                        <li class="nav-item"><a class="nav-item-signin nav-item-hover" href="<?= base_url('register') ?>">Register</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>