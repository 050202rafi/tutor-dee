<!DOCTYPE html>
<html lang="en" class="no-js">

<!--AWAL HEAD-->
<?= $this->include('layout/head') ?>
<!-- AKHIR HEAD-->

<!-- BODY -->
<body>
    <!--========== HEADER ==========-->
    <?= $this->include('layout/header') ?>
    <!--========== AKHIR HEADER ==========-->

    <!--========== PAGE LAYOUT ==========-->
    <?= $this->renderSection('content') ?>
    <!--========== FOOTER ==========-->
    <?= $this->include('layout/footer') ?>
    <!--========== AKHIR FOOTER ==========-->

    <!-- Back To Top -->
    <a href="javascript:void(0);" class="js-back-to-top back-to-top">Top</a>

    <!-- JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- CORE PLUGINS -->
    <script src=<?= base_url('jquery/jquery.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery-migrate.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('bootstrap/js/bootstrap.min.js') ?> type="text/javascript"></script>

    <!-- PAGE LEVEL PLUGINS -->
    <script src=<?= base_url('jquery/jquery.easing.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery.back-to-top.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery.smooth-scroll.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery.wow.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery.parallax.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('swiper/js/swiper.jquery.min.js') ?> type="text/javascript"></script>

    <!-- PAGE LEVEL SCRIPTS -->
    <script src=<?= base_url('js/layout.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('js/components/wow.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('js/components/swiper.min.js') ?> type="text/javascript"></script>

</body>
<!-- AKHIR BODY -->

</html>