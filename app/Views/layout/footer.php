<footer class="footer">
    <!-- Link -->
    <div class="footer-seperator">
        <div class="content-lg container">
            <div class="row">
                <div class="col-sm-2 sm-margin-b-50">
                    <!-- List -->
                    <ul class="list-unstyled footer-list">
                        <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url() ?>>Home</a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('tutorials') ?>>Tutorial</a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('about') ?>>About</a></li>
                    </ul>
                </div>
                <div class="col-sm-4 sm-margin-b-30">
                    <!-- List -->
                    <ul class="list-unstyled footer-list">
                        <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('https://twitter.com') ?>>Twitter</a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('https://facebook.com') ?>>Facebook</a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('https://instagram.com') ?>>Instagram</a></li>
                        <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('https://youtube.com') ?>>YouTube</a></li>
                    </ul>

                </div>
                <div class="col-sm-5 sm-margin-b-30">
                    <h2 class="color-white">Send Us A Note</h2>
                    <input type="text" class="form-control footer-input margin-b-20" placeholder="Name" required>
                    <input type="email" class="form-control footer-input margin-b-20" placeholder="Email" required>
                    <input type="text" class="form-control footer-input margin-b-20" placeholder="Phone" required>
                    <textarea class="form-control footer-input margin-b-30" rows="6" placeholder="Message" required></textarea>
                    <a href=<?= base_url() ?>><button type="submit" class="btn-theme btn-theme-sm btn-base-bg text-uppercase">Submit</button></a>
                </div>
            </div>

        </div>
    </div>
    <!-- Link -->
    <div class="content container">
    </div>
</footer>