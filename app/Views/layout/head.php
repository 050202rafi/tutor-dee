<head>
    <meta charset="utf-8" />
    <title>Tutor DEE</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('simple-line-icons/simple-line-icons.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" />

    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link rel="stylesheet" href="<?= base_url('css/animate.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('swiper/css/swiper.min.css') ?>" />

    <!-- THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('css/layout.min.css') ?>" />

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?= base_url('img/logo-dark.png') ?>" />
</head>