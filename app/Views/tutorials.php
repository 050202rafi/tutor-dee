<?php
if (!isset($_SESSION['username'])) {
    header("Location: beranda.php");
}
?>

<!DOCTYPE html>
<html lang="en" class="no-js">
<!--AWAL HEAD-->

<head>
    <meta charset="utf-8" />
    <title>Tutor DEE</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('simple-line-icons/simple-line-icons.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" />

    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link rel="stylesheet" href="<?= base_url('css/animate.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('swiper/css/swiper.min.css') ?>" />

    <!-- THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('css/layout.min.css') ?>" />

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?= base_url('img/logo-dark.png') ?>" />
</head>
<!-- AKHIR HEAD-->

<!-- BODY -->

<body>
    <!--========== HEADER ==========-->
    <header class="header navbar-fixed-top">
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="menu-container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="toggle-icon"></span>
                    </button>

                    <!-- Logo -->
                    <div class="logo">
                        <a class="logo-wrap" href="<?= base_url() ?>">
                            <img class="logo-img logo-img-main" src="<?= base_url('img/logo.png') ?>" alt="Asentus Logo">
                            <img class="logo-img logo-img-active" src="<?= base_url('img/logo-dark.png') ?>" alt="Asentus Logo">
                        </a>
                    </div>
                </div>

                <div class="collapse navbar-collapse nav-collapse">
                    <div class="menu-container">
                        <ul class="navbar-nav navbar-nav-right">
                            <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url() ?>">Home</a></li>
                            <li class="nav-item"><a class="nav-item-child nav-item-hover active" href="<?= base_url('tutorials') ?>">Tutorial</a></li>
                            <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url('about') ?>">About</a></li>
                            <li class="nav-item"><a class="nav-item-login nav-item-hover" href="<?= base_url('login') ?>">Login</a></li>
                            <li class="nav-item"><a class="nav-item-signin nav-item-hover" href="<?= base_url('register') ?>">Register</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!--========== AKHIR HEADER ==========-->

    <!--========== PARALLAX ==========-->
    <div class="parallax-window" data-parallax="scroll" data-image-src="<?= base_url('/img/1920x1080/01.jpg') ?>">
        <div class="parallax-content container">
            <h1 class="carousel-title">Tutorial</h1>
            <p>Cari panduan yang kamu butuhkan di sini!</p>
        </div>
    </div>
    <!--========== PARALLAX ==========-->

    <!--========== PAGE LAYOUT ==========-->
    <div class="content-nxt container">
        <div class="row margin-b-40">
            <div class="col-sm-6">
                <h2>Rekomendasi</h2>
            </div>
        </div>


        <div class="row margin-b-50">
            <?= view_cell('\App\Libraries\Widget::recentPost') ?>
        </div>
    </div>

    <!--========== FOOTER ==========-->
    <footer class="footer">
        <!-- Link -->
        <div class="footer-seperator">
            <div class="content-lg container">
                <div class="row">
                    <div class="col-sm-2 sm-margin-b-50">
                        <!-- List -->
                        <ul class="list-unstyled footer-list">
                            <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url() ?>>Home</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('tutorials') ?>>Tutorial</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('about') ?>>About</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4 sm-margin-b-30">
                        <!-- List -->
                        <ul class="list-unstyled footer-list">
                            <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('https://twitter.com') ?>>Twitter</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('https://facebook.com') ?>>Facebook</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('https://instagram.com') ?>>Instagram</a></li>
                            <li class="footer-list-item"><a class="footer-list-link" href=<?= base_url('https://youtube.com') ?>>YouTube</a></li>
                        </ul>

                    </div>
                    <div class="col-sm-5 sm-margin-b-30">
                        <h2 class="color-white">Send Us A Note</h2>
                        <input type="text" class="form-control footer-input margin-b-20" placeholder="Name" required>
                        <input type="email" class="form-control footer-input margin-b-20" placeholder="Email" required>
                        <input type="text" class="form-control footer-input margin-b-20" placeholder="Phone" required>
                        <textarea class="form-control footer-input margin-b-30" rows="6" placeholder="Message" required></textarea>
                        <a href=<?= base_url() ?>><button type="submit" class="btn-theme btn-theme-sm btn-base-bg text-uppercase">Submit</button></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- Link -->
    </footer>
    <!--========== AKHIR FOOTER ==========-->

    <!-- Back To Top -->
    <a href="javascript:void(0);" class="js-back-to-top back-to-top">Top</a>

    <!-- JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- CORE PLUGINS -->
    <script src=<?= base_url('jquery/jquery.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery-migrate.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('bootstrap/js/bootstrap.min.js') ?> type="text/javascript"></script>

    <!-- PAGE LEVEL PLUGINS -->
    <script src=<?= base_url('jquery/jquery.easing.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery.back-to-top.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery.smooth-scroll.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery.wow.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('jquery/jquery.parallax.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('swiper/js/swiper.jquery.min.js') ?> type="text/javascript"></script>

    <!-- PAGE LEVEL SCRIPTS -->
    <script src=<?= base_url('js/layout.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('js/components/wow.min.js') ?> type="text/javascript"></script>
    <script src=<?= base_url('js/components/swiper.min.js') ?> type="text/javascript"></script>

</body>
<!-- AKHIR BODY -->

</html>