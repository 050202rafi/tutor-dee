<?php
if (!isset($_SESSION['username'])) {
    header("Location: beranda.php");
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Tutor DEE</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- GLOBAL MANDATORY STYLES -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('bootstrap/css/bootstrap.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('simple-line-icons/simple-line-icons.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Hind:300,400,500,600,700" />

    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link rel="stylesheet" href="<?= base_url('css/animate.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('swiper/css/swiper.min.css') ?>" />

    <!-- THEME STYLES -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('css/layout.min.css') ?>" />
    <link rel="stylesheet" type="text/css" href="<?= base_url('ckeditor/css/style.css') ?>" />

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/png" href="<?= base_url('img/logo-dark.png') ?>" />
</head>

<!--== HEADER ==-->

<body>
    <header class="header navbar-fixed-top" style="background: url(../img/1920x1080/02.jpg) no-repeat; background-size: cover; background-position: center center;">
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="menu-container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="toggle-icon"></span>
                    </button>

                    <!-- Logo -->
                    <div class="logo">
                        <a class="logo-wrap" href="<?= base_url('beranda') ?>">
                            <img class="logo-img logo-img-main" src="<?= base_url('img/logo.png') ?>" alt="Asentus Logo">
                            <img class="logo-img logo-img-active" src="<?= base_url('img/logo-dark.png') ?>" alt="Asentus Logo">
                        </a>
                    </div>
                </div>

                <div class="collapse navbar-collapse nav-collapse">
                    <div class="menu-container">
                        <ul class="navbar-nav navbar-nav-right">
                            <li class="nav-item"><a class="nav-item-login nav-item-hover" href="<?= base_url('tutorialku') ?>">Tutorialku</a></li>
                            <li class="nav-item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                            <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url('beranda') ?>">Home</a></li>
                            <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url('tutorafterlogin') ?>">Tutorial</a></li>
                            <li class="nav-item"><a class="nav-item-child nav-item-hover" href="<?= base_url('tentang') ?>">About</a></li>
                            <li class="nav-item"><a class="nav-item-signin nav-item-hover active" href="<?= base_url('info') ?>">Hai! <?= session()->get('nama'); ?></a></li>
                            <li class="nav-item"><a class="nav-item-logout nav-item-hover" href="<?= base_url('logout') ?>">Log Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <!-- Navbar -->
    <div class="container-fluid" style="margin-top: 125px;">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <?php if (!empty(session()->getFlashdata('error'))) : ?>
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <?php echo session()->getFlashdata('error'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </div>
                <?php endif; ?>
                <?php if (session()->getFlashdata('success')) : ?>
                    <div class="alert alert-success alert-dismissible fade in" role="alert">
                        <?php echo session()->getFlashdata('success'); ?>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <div style="border:0; padding:85px; width:760px; height:auto;">
        <?= csrf_field(); ?>
        <form action="<?= base_url(); ?>/Info/update" method="POST" name="form-input-data">
            <table width="760" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr height="46">
                    <td width="10%"></td>
                    <td width="25%"></td>
                    <td width="65%"></td>
                </tr>

                <input type="hidden" value="<?php echo $_SESSION['username'] ?>" name="username">
                </tr>
                <tr height="75">
                    <td> </td>
                    <td>Nama</td>
                    <td>
                        <input type="text" name="nama" placeholder="Masukkan nama baru" id="nama" class="form-control" size="35">
                    </td>
                </tr>
                <tr height="75">
                    <td> </td>
                    <td>Email</td>
                    <td>
                        <input type="email" placeholder="Masukkan email baru" name="email" id="email" class="form-control" size="35">
                    </td>
                </tr>
                <tr height="75">
                    <td> </td>
                    <td>Password</td>
                    <td>
                        <input type="password" placeholder="Masukkan password baru" name="password" id="password" class="form-control" size="35">
                    </td>
                </tr>
                <tr height="80">
                    <td> </td>
                    <td> </td>
                    <td><button name="submit" class="btn-theme btn-theme-sm btn-base-bg text-uppercase">Simpan</button>
                        <button class="btn-theme btn-theme-sm btn-base-bg text-uppercase" style="background-color: red; margin: 20px;" href="<?= base_url('beranda') ?>">Batal</button>
                    </td>
                </tr>
            </table>
        </form>
    </div>


    <a href="javascript:void(0);" class="js-back-to-top back-to-top">Top</a>
    <!--===== MAIN JS =====-->
    <!-- JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
    <!-- CORE PLUGINS -->
    <script src="<?= base_url('js/main.js') ?>"></script>
    <script src="<?= base_url('jquery/jquery.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('jquery/jquery-migrate.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>

    <!-- PAGE LEVEL PLUGINS -->
    <script src="<?= base_url('jquery/jquery.easing.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('jquery/jquery.back-to-top.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('jquery/jquery.smooth-scroll.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('jquery/jquery.wow.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('masonry/imagesloaded.pkgd.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('masonry/jquery.masonry.pkgd.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('swiper/js/swiper.jquery.min.js') ?>" type="text/javascript"></script>

    <!-- PAGE LEVEL SCRIPTS -->
    <script src="<?= base_url('js/layout.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('js/components/wow.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('js/components/swiper.min.js') ?>" type="text/javascript"></script>
    <script src="<?= base_url('js/components/masonry.min.js') ?>" type="text/javascript"></script>
</body>

</html>