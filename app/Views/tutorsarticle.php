<?= $this->extend('layout/web_layout') ?>
<?= $this->section('content') ?>
<?= csrf_field(); ?>
<?php foreach ($builder->getResult() as $q) { ?>
    <div class="content-lg container">
        <div class="row margin-t-100">
            <h1><?php echo $q->judul ?></h1>
            <h5><span class="text-uppercase margin-l-15">Oleh : <?php echo $q->author ?></span></h5>
            <h5><span class="text-uppercase margin-l-15"><?php echo $q->tag ?></span></h5>
        </div>
        <img class="img-responsive" src="<?= base_url('img/tutor/' . $q->gambar . '') ?>">
        <div class="row margin-b-50">
            <div class="margin-t-50">
                <?php echo $q->isi ?>
            </div>
        </div>
    </div>
    <div id="disqus_thread"></div>
    <script>
        /**
         *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
         *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables    */
        /*
        var disqus_config = function () {
        this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
        this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
        };
        */
        (function() { // DON'T EDIT BELOW THIS LINE
            var d = document,
                s = d.createElement('script');
            s.src = 'https://tutordee.disqus.com/embed.js';
            s.setAttribute('data-timestamp', +new Date());
            (d.head || d.body).appendChild(s);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
<?php } ?>
<?= $this->endSection() ?>