<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (is_file(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
$routes->get('/tutorials', 'Page::tutorials');
$routes->get('/article/(:any)', 'Page::article/$1');
$routes->get('/about', 'Page::about');
$routes->get('/login', 'LoginController::index');
$routes->get('/register', 'Register::index');
$routes->match(['get', 'post'], 'Register/save', 'Register::save');
$routes->match(['get', 'post'], 'LoginController/loginAuth', 'LoginController::loginAuth');
$routes->get('/beranda', 'Beranda::index',['filter' => 'authGuard']);
$routes->get('/tutorialku', 'Tutorialku::tutorialku',['filter' => 'authGuard']);
$routes->get('/tutorafterlogin', 'Page::tutorafterlogin',['filter' => 'authGuard']);
$routes->get('/tutorsarticle/(:any)', 'Page::tutorsarticle/$1',['filter' => 'authGuard']);
$routes->get('/tentang', 'Page::tentang',['filter' => 'authGuard']);
$routes->get('/info', 'Info::info',['filter' => 'authGuard']);
$routes->get('/logout', 'LoginController::logout');
$routes->match(['get', 'post'], 'Info/update', 'Info::update');
$routes->match(['get', 'post'], 'Tutorialku/save', 'Tutorialku::save');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
