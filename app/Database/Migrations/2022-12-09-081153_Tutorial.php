<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Tutorial extends Migration
{
    public function up()
    {
        //
        $this->forge->addField([
            'tutorid'               => [
                'type'              => 'INT',
                'constraint'        => '12',
                'unsigned'          => true,
                'auto_increment'    => true,
            ],
            'judul'                 => [
                'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'author'                => [
                'type'              => 'VARCHAR',
                'constraint'        => '12',
            ],
            'isi'                   => [
                'type'              => 'TEXT',
            ],
            'gambar'                => [
				'type'              => 'VARCHAR',
                'constraint'        => '100',
            ],
            'tag'                   => [
				'type'              => 'VARCHAR',
				'constraint'        => '12',
            ],
            'created_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
			],
			'updated_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
            ],
            'deleted_at' => [
				'type'           => 'DATETIME',
				'null'       	 => true,
            ],

        ]);
        $this->forge->addPrimaryKey('tutorid');
        $this->forge->createTable('tutorial');
    }

    public function down()
    {
        //
        $this->forge->dropTable('tutorial');
    }
}
