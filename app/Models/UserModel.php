<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model
{
    protected $table      = 'users';
    protected $primaryKey = 'username';
    protected $returnType = "object";
    protected $useTimestamps = true;
    protected $allowedFields = ['username', 'nama','email','password'];
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
}