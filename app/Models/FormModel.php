<?php

namespace App\Models;

use CodeIgniter\Model;

class FormModel extends Model
{
    protected $table      = 'tutorial';
    protected $primaryKey = 'tutorid';
    protected $returnType = "object";
    protected $useTimestamps = true;
    protected $allowedFields = ['judul','author','isi','gambar','tag'];
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';
}